import java.util.concurrent.CountDownLatch;

//starts (sleeps for 200ms) does something (sleeps for 20ms)
//finishes & reduces the countDownLatch
public class Client implements Runnable {
	    private int m_id;
	    private CountDownLatch m_latchObject;
	 
	    public Client(int id, CountDownLatch latchObject){
	        m_id = id;
	        m_latchObject = latchObject;
	    }
	    
	    public void run (){
	        // Client  5
	    	System.out.println("Client #" + m_id + " started");
	 
	        try {
	            Thread.sleep(200);
	        } catch (InterruptedException e) {}
	        
	        // Client is working
	        System.out.println("Client #" + m_id + " reports it is doing something");
	        
	        
	        try {
	            Thread.sleep(200);
	        } catch (InterruptedException e) {}
	                
	        // Client is shutting down
	        System.out.println("Client #" + m_id + " finished");
	                
	        // Client decreases the count by one
	        m_latchObject.countDown();
	    }
}