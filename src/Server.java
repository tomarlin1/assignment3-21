import java.util.concurrent.CountDownLatch;

//server initialises and waits for the CountDownLatch to become zero then exits
public class Server implements Runnable {
	private CountDownLatch m_latchObject;

	public Server(CountDownLatch latchObject){
		m_latchObject = latchObject;
	}

	public void run (){
		System.out.println("Server initialized... waiting for clients");
		
		try {
			// Waiting for the clients to bring the counter to 0
			m_latchObject.await();
		} catch (InterruptedException e){
			return;
		}

		System.out.println("Server finished");
	}
}
