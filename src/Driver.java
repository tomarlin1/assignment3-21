import java.util.concurrent.CountDownLatch;

public class Driver {
	/**
	 * 
	 * We have 3 client threads and one server. The server starts. It
	 * finish only when the clients have finished.
	 */

	public static void main(String[] args){

		// A latch object clients decrease - it is shared by the server and clients
		// The server waits until it is 0.
		CountDownLatch latchObject = new CountDownLatch (1);

		Server s =  new Server (latchObject);
		Client c1 = new Client (1, latchObject);
		Client c2 = new Client (2, latchObject);
		Client c3 = new Client (3, latchObject);

		Thread ts=new Thread(s);
		Thread tc1=new Thread(c1);
		Thread tc2=new Thread(c2);
		Thread tc3=new Thread(c3);
		// Running the server and the clients
		ts.start();
		tc1.start();
		tc2.start();  
		tc3.start();
	}
}

